
This repository accompanies the paper

# SPECTRE - A novel dMRI visualization technique for the display of cerebral connectivity

Marco Reisert, Ph.D , Christoph Kaller, Ph.D , Marvin Reuter, Horst Urbach, Bastian E. Sajonz, Peter C. Reinacher, Volker A. Coenen
 


### Overview ###
The approach, named SPECTRE - Subject sPEcific brain Connectivity display in the Target REgion, is related to track-weighted imaging, where diffusion MRI streamlines are used to aggregate information from a different MRI contrast. Instead of using native MRI contrasts, we propose here to use continuous, template information as the underlying contrast for aggregation. 

### Requirements

The code requires MATLAB (>R2009). The repository provides the core tract-weighting algorithm, written in C++ (mex) plugin. We provide 
compiled version for Linux and Windows (64bit). The algorithm takes  inputs as NIFITs. It is assumed that all NIFTIs live in the 
same space (typically the native space of the subject). The inputs are:

* **Peak orientations**. The orientations come typically from a CSD approach. The orientation are given as a 4D NIFTI (w,h,d,3*nd) with the direction encoded in the 4th dimension. For example, when using MRtrix use 
`dwi2response` and `dwi2fod` to estimate the the fiber orientation distribution (FOD) and use `sh2peaks $fod $peaks` to turn the FOD into a peak distribution. 
* **Color coding**. We provide the color coding used in the paper as a NIFTI in MNI space (coloring_scheme.nii.gz). To run the algorithm it is necessary to warp the volume into subject's space, which is registered with the space of the DWI. Depending on the normalization algorithm used (e.g. CAT12 from SPM, MRtrix, FSL) there are different ways to warp the color coding. We refer to the documenations of the corresponding packages.
* **Target mask**. The mask defines where the SPECTRE contrast is computed. We provide the mask in MNI space (target_midbrain.nii.gz),  i.e. you need to warp the mask appropriatly to subject's space.
* **Tracking mask**. Typically coming from a WM/GM segmentation based an subject specific T1. 
### Example ###
To test SPECTRE on the example data provided in the repository, run the following piece of code in matlab:
```
%% input niftis
dirfield = 'multidir3_fod.nii.gz';
coloring = 'coloring_scheme.nii.gz';
targetregion = 'target_midbrain.nii.gz';
trackingmask = 'tracking_mask.nii.gz';
outname = 'spectre_map_demo.nii.gz';
%% run SPECTRE
res = spectralize(dirfield,trackingmask,targetregion,coloring,outname, ...
               'nwalker',100);
```
This will produce a medium quality SPECTRE map in about 10 minutes on a standard Desktop PC.
Additional parameters (like oversampling factor) are given as key value pairs to spectralize.

```
% these are the default parameters
upsamp = 2;        % spatial upsample factor (relative to mdir spaceing)
step = 1;          % stepwidth in mm
maxfiblen = 1000;  % maximal length of streamlines
nwalker = 1000;    % number of walkers per seed
maxang = 80;       % maximal angle
sigma = 0.1;       % noise factor (in units of the directional peaks)

thres_seed = 0.3;  % if the given seed mask contains non-binary information ...
scale = 1;         % scaling of mdir volume
normalize = 1;     % normalization of mdir volume (scale parameter becomes useless)

```


### Contact ###

* Marco Reisert (marco.reisertATuniklinik-freiburg.de)
