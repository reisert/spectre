
#include "mex.h"
#include "matrix.h"
#include <math.h>
#include <string.h>
#include <vector>
#include "MersenneTwister.h"
#define REAL float

using namespace std;

MTRand mtrand;

/* tracking parameters*/
struct Parameters
{
   REAL alpha;        // step length
   int maxfiblen;     // maximal length of fibers
   int seeding;       // number of seeds per voxel
   int reversible;    // NAN
   int norevisits;    // NAN
   int lenbias;       // accumulate proportional to length (mitigates length bias a bit)
   REAL maxcos;       // maximal angle cutoff (given as cos(angle))
   REAL sigma;        // stddev of noise component
};

struct Proposal
{    
    REAL dot;
};



/* *************************************************************************************************************
 * 
 * REAL determine_new_direction(REAL *df,REAL *olddir,REAL *newdir,Parameters *params, int start,int maxdir)
 * 
 * draws new direction according to x_(n+1) = x_(n) + u + noise ,
 * where u is the direction d_f with largest dot product, which does not exceed the angle criterion 
 * i.e. u = argmax_(d_f) abs(d_f*x_(n)) where abs(d_f*x_(n)) > maxcos
 * and noise is zero-mean Gaussian distributed 
 *
 * df - array of new directions of size [3 maxdir]
 * olddir - last direction 
 * newdir - drawn direction 
 * params - tracking parameters
 * start  -  flag, whether olddir is a valid dir (start>1) or we are at a seedpoint (start=1)
 * 
 * returns whether new direction is successful assigned, otherwise -1
 *
*/


REAL determine_new_direction(REAL *df,REAL *olddir,REAL *newdir,Parameters *params, int start,int maxdir)
{

        REAL sig = params->sigma;
        
        int maxidx = -1; REAL maxdot = 0;
        if (start == 1)
        {
        
            for (int k = 0; k < maxdir; k++)
            {
                REAL dx = df[3*k];
                REAL dy = df[3*k+1];
                REAL dz = df[3*k+2];
                REAL dot = (olddir[0]*dx + olddir[1]*dy + olddir[2]*dz);
                if (fabs(maxdot) < fabs(dot))
                {
                    maxdot = dot;
                    maxidx = k;
                }

            }
        }
        else
        {
            maxidx = 0;
            maxdot = (rand()%2 == 1)?1:-1;
        }
        
        Proposal p;
        p.dot = -1;
        if (maxidx == -1)
            return -1;            
        if (fabs(maxdot) < params->maxcos)
            return -1;
        
        REAL dx = df[3*maxidx]    + mtrand.frandn()*sig;
        REAL dy = df[3*maxidx+1]  + mtrand.frandn()*sig;
        REAL dz = df[3*maxidx+2]  + mtrand.frandn()*sig;
        REAL n = sqrt(dx*dx+dy*dy+dz*dz);
        dx /= n; dy /= n; dz /= n;
        REAL sg = (maxdot>0)?1.0:-1.0;
              
        newdir[0] =  dx*sg;
        newdir[1] =  dy*sg;
        newdir[2] =  dz*sg;
                       
        return fabs(maxdot);
        
}




/* *************************************************************************************************************
 * 
 * track(REAL *result,REAL *dfield, REAL *mask,REAL *coloring, int sidx,int w, int h,int d,int ncol, int maxdir, Parameters *params)
 * 
 * does the actual path integration 
 *
 * result - result array if weighted visitmaps of size [w h d 3+ncol], where the volumes contain
 *          0) the raw visitcount map 
 *          1) length bias adjusted visitmap
 *          2) unassigned
 *          3 - 3+ncol) contrast weighted
 * dfield - array with tracking directions of size [3*maxdir w h d]
 * mask   - a general stopping mask 
 * sidx   - index of seed according to array size [w h d]
 * w,h,d  - sizes of 3D array
 * ncol   - number of wighting contrasts
 * maxdir - number of directions in dfield

*/


void track(REAL *result,REAL *dfield, REAL *mask,REAL *coloring, int sidx,int w, int h,int d,int ncol, int maxdir, Parameters *params)
{
    
    int x,y,z;
    x = sidx % w;
    y = (sidx/w) % h;
    z = sidx/(w*h);
 
    REAL newdir[3];
    REAL curdir[3];
    int curdir_id,newdir_id;
    REAL alpha= params->alpha;
    
    int len = 0;
    
    int whd = w*h*d;
    
    
    if (x<0 || x >= w || y < 0  || y >= h || z < 0 || z >= d)
        return;
    if (mask[w*h*z + w*y + x] == 0)
        return;

    curdir[0] = mtrand.frandn();
    curdir[1] = mtrand.frandn();
    curdir[2] = mtrand.frandn();
    
    if (determine_new_direction(dfield + (w*h*z + w*y + x)*maxdir*3,curdir,newdir,params,-1,maxdir) == -1)
        return;
    
    REAL xf = mtrand.frand()+x;
    REAL yf = mtrand.frand()+y;
    REAL zf = mtrand.frand()+z;
   

    REAL xf_new;
    REAL yf_new;
    REAL zf_new;
    int x_new;
    int y_new;
    int z_new;
        
    
    REAL col[ncol];
    for (int k = 0; k < ncol;k++)
        col[k] = 0; 
    
    while(len < params->maxfiblen)
    {
       
        // draw new direction        
        if (determine_new_direction(dfield + (w*h*z + w*y + x)*maxdir*3,curdir,newdir,params,1,maxdir) == -1)
            break;
        
        
        // track forward
        xf_new = xf + alpha*newdir[0];
        yf_new = yf + alpha*newdir[1];
        zf_new = zf + alpha*newdir[2];                
        x_new = floor(xf_new);
        y_new = floor(yf_new);
        z_new = floor(zf_new);
        if (x_new<0 || x_new >= w || y_new < 0  || y_new >= h || z_new < 0 || z_new >= d)
           break;   
        if (mask[w*h*z_new + w*y_new + x_new] == 0)
            break;        
        
        // accumulate probmap
        result[w*h*z_new + w*y_new + x_new]++;

        // accumulate length ajdusted probmap
        result[whd + w*h*z_new + w*y_new + x_new]+= REAL(len); 

        // accumulate other contrasts/colors
        for (int k = 0; k < ncol;k++)        
            col[k] += coloring[w*h*z_new + w*y_new + x_new + k*whd ];
        
        
        // swap
        xf = xf_new;
        yf = yf_new;
        zf = zf_new;
        x = x_new;
        y = y_new;
        z = z_new;
        curdir[0] = newdir[0];
        curdir[1] = newdir[1];
        curdir[2] = newdir[2];
        
        
        len++;
    }
    
    for (int k = 0; k < ncol;k++)        
        result[whd*(3+k) + sidx] += col[k];

    
}


void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
 	
    if(nrhs != 5) {
        

        printf("\nUsage: res = probtrack_color(dfield, mask, contrasts, seed_index, paramarray); \n\n",nrhs);
        printf(" Computes the contrast-weighted probablistic tractographies\n\n");
        printf("  dfield - array of sizes [3*maxdir w h d] \n");
        printf("  mask - a mask where to track [w h d] \n");
        printf("  contrasts - N contrast images, i.e. size [w h d N]\n");
        printf("  seed_index - a flat array of linear indices representing seed positions\n");
        printf("  paramarray - a array containing parameters\n");
        printf("             [maxfiblen,seeding,alpha,maxcos,sigma]\n");
        printf("                alpha       step length \n");
        printf("                maxfiblen   maximal length of fibers \n");
        printf("                seeding     number of seeds per voxel \n");
        printf("                maxcos      maximal angle cutoff (given as cos(angle)) \n");
        printf("                sigma       stddev of noise component \n");
        printf("\n all parameters have to be of type double!!\n\n");
  
        return;
	} else if(nlhs>1) {
	printf("Too many output arguments\n");
    return;
	}

    int pcnt = 0;
    const mxArray *Dfield;
    Dfield = prhs[pcnt++];       
    const int numdim = mxGetNumberOfDimensions(Dfield);
    const int *dims = mxGetDimensions(Dfield);
    REAL *dfield= (REAL*) mxGetData(Dfield);

    int w,h,d,maxdir;    
    maxdir = dims[0]/3;
    w = dims[1];
    h = dims[2];
    d = dims[3];   

    
    const mxArray *Mask;
    Mask = prhs[pcnt++];       
    REAL *mask = (REAL*) mxGetData(Mask);

    
    const mxArray *Coloring;
    Coloring = prhs[pcnt++];       
    REAL *coloring = (REAL*) mxGetData(Coloring);

    const int *coldims = mxGetDimensions(Coloring);
    int ncol = coldims[3];

    
    const mxArray *Seeds;
    Seeds = prhs[pcnt++];
    double *seeds = (double*) mxGetData(Seeds); //// todo
    int numseeds = mxGetM(Seeds);
    
    const mxArray *Params;
    Params = prhs[pcnt++];
    REAL *par = (REAL*) mxGetData(Params);

    Parameters params;
    params.maxfiblen = (int) par[0];
    params.seeding = (int) par[1]; 
    params.alpha = (REAL) par[2];  
    params.maxcos = (REAL) par[3];
    params.sigma = (REAL) par[4];
  
    int newdim[4];
    newdim[0] = dims[1];
    newdim[1] = dims[2];
    newdim[2] = dims[3];
    newdim[3] = ncol+3;
    
    plhs[0] = mxCreateNumericArray(4,newdim,mxGetClassID(Dfield),mxREAL);
    REAL *result = (REAL*) mxGetData(plhs[0]);


    for (int k = 0; k < numseeds ;k++)
    {
            
        if (k%(numseeds/100)==0)
        {
            fprintf(stderr,"completed %.2f%%\n",double(k)/double(numseeds)*100);
            fflush(stderr);
        }
        
        int sidx = (int) seeds[k];
       
        for (int numtry = 0; numtry < params.seeding; numtry++)     
        {
            track(result,dfield,mask,coloring,sidx,w,h,d,ncol,maxdir,&params);                
        }
       
  
    }
            
        
    
}





