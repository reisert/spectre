


function nii = DPX_cropnifti(nii,bbox,fac)

nii.edges = [nii.hdr.hist.srow_x; nii.hdr.hist.srow_y; nii.hdr.hist.srow_z; 0 0 0 1];

if isstruct(bbox), % a mask defining the bbox
    
    
    if not(exist('fac'))
        fac = 1;
    end,
    
    bbox = DPX_reslice_volume(bbox,nii,1);
    sz = size(bbox.img);
   %%
    im = bbox.img;
    imx = find(sum(sum(im>0,2),3))-1;
    imy = find(sum(sum(im>0,1),3))-1;
    imz = find(sum(sum(im>0,2),1))-1;
    a = [ imx(1) imx(end) ;
         imy(1) imy(end) ;
         imz(1) imz(end) ;
         1 1];        
    b = inv(nii.edges) * bbox.edges * a;
    
    low = min(b');
    up = max(b');
    dif = up-low;
    low = low - dif*(fac-1);
    up = up + dif*(fac-1);
    bbox = nii.edges*[low' up'];
    
    
end;


bboxvox = (floor(inv(nii.edges)*bbox)')';
% 
% ssize = [0 0 0];
% for k = 1:3,
%     if bboxvox(k,1) < 1,
%          psize = [0 0 0];
%          psize(k) = 1-bboxvox(k,1);
%          nii.img = padarray(nii.img,psize,0,'pre');
%          bboxvox(k,:) = bboxvox(k,:) + psize(k);
%          ssize = ssize + psize;
%      end
%     if bboxvox(k,2) > size(nii.img,k)
%          psize = [0 0 0];
%          psize(k) = bboxvox(k,2)-size(nii.img,k);
%          nii.img = padarray(nii.img,psize,0,'post');
%      end
% end

%bbox(:,1) = bbox(:,1) - (nii.edges*[ssize' ; 1] - nii.edges*[[0 0 0]' ; 1]);

bbox = nii.edges*bboxvox;


nii.hdr.hist.srow_x(4) = bbox(1,1);
nii.hdr.hist.srow_y(4) = bbox(2,1);
nii.hdr.hist.srow_z(4) = bbox(3,1);

% 
% if bboxvox(3,1)> bboxvox(3,2)
%     nii.hdr.hist.srow_z(4) = bbox(3,2);
%     ran_z = bboxvox(3,2)+2:bboxvox(3,1)+2;
% else
%     nii.hdr.hist.srow_z(4) = bbox(3,1);    
%     ran_z = bboxvox(3,1):bboxvox(3,2);
% end;



bboxvox = bboxvox+1;

for k = 1:3,
    if bboxvox(k,1) < 1,
        ps = [0,0,0];
        pad = 1-bboxvox(k,1);
        ps(k) = pad;
        nii.img = padarray(nii.img,ps,0,'pre');
        bboxvox(k,1) = bboxvox(k,1) + pad;
        bboxvox(k,2) = bboxvox(k,2) + pad;
    end
    if bboxvox(k,2) > size(nii.img,k),
        ps = [0,0,0];
        pad = bboxvox(k,2)-size(nii.img,k)
        ps(k) = pad;
        nii.img = padarray(nii.img,ps,0,'post');
        
    end
end

nii.img = nii.img(bboxvox(1,1):bboxvox(1,2),bboxvox(2,1):bboxvox(2,2),bboxvox(3,1):bboxvox(3,2,:,:,:),:);

sz = size(nii.img);
nii.hdr.dime.dim(2:4) = sz(1:3);

nii.edges = [nii.hdr.hist.srow_x;nii.hdr.hist.srow_y;nii.hdr.hist.srow_z;0 0 0 1];


