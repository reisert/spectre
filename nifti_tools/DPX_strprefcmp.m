function r = DPX_strprefcmp(pref,str)

    n = length(pref);
    m = length(str);
    r = strcmp(pref,str(1:min(m,n)));
 
        
