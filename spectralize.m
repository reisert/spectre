
function result_cropped = spectralize(dirfield,trackingmask,targetregion,coloring,outname,varargin)

mdir_field_low = load_untouch_nii(dirfield);


mask = load_untouch_nii(trackingmask);
seedmask = load_untouch_nii(targetregion);
coloring =  load_untouch_nii(coloring);



% threse are the default parameters
upsamp = 2;        % spatial upsample factor (relative to mdir spaceing)
step = 1;          % stepwidth in mm
maxfiblen = 1000;  % maximal length of streamlines)
nwalker = 1000;       % number of walkers per seed
maxang = 80;       % maximal angle
sigma = 0.1;       % noise factor (in units of the directional peaks)
gamma = 1;         % exp. length factor

thres_seed = 0.3;  % if the given seed mask contains non-binary information ...
scale = 1;         % scaling of mdir volume
normalize = 1;     % normalization of mdir volume (scale parameter becomes useless)

T = eye(3);

% overload parameters
for k = 1:2:length(varargin),
     eval([varargin{k} '=varargin{k+1};']);
end;

maxcos = cos(maxang/180*pi);


%% convert to voxelgrid for MRstrix type mdirs
if DPX_strprefcmp('MRtrix',mdir_field_low.hdr.hist.descrip),
    for k = 1:size(mdir_field_low.img,4)/3,
        R = (T * diag(1./mdir_field_low.voxsz) * mdir_field_low.edges(1:3,1:3))';
        mdir_field_low.img(:,:,:,(3*(k-1)+1):(3*k)) = ...
            cat(4, mdir_field_low.img(:,:,:,(3*(k-1)+1))*R(1,1) + mdir_field_low.img(:,:,:,(3*(k-1)+1)+1)*R(1,2) + mdir_field_low.img(:,:,:,(3*(k-1)+1)+2)*R(1,3),...
                   mdir_field_low.img(:,:,:,(3*(k-1)+1))*R(2,1) + mdir_field_low.img(:,:,:,(3*(k-1)+1)+1)*R(2,2) + mdir_field_low.img(:,:,:,(3*(k-1)+1)+2)*R(2,3), ...
                   mdir_field_low.img(:,:,:,(3*(k-1)+1))*R(3,1) + mdir_field_low.img(:,:,:,(3*(k-1)+1)+1)*R(3,2) + mdir_field_low.img(:,:,:,(3*(k-1)+1)+2)*R(3,3));                   
    end
end;

%% upsample to higher resolution
if upsamp > 1,
    mdir_field = DPX_reslice_volume(mdir_field_low,upsamp,'nearest');
else
    mdir_field = mdir_field_low;
end;
template = mdir_field;


%% reslice of masks/coloring to gridding of orientation field
display('reslicing')
mask = DPX_reslice_volume(mask,mdir_field,1);
seedmask = DPX_reslice_volume(seedmask,mdir_field,1); 
coloring = DPX_reslice_volume(coloring,mdir_field,1); 
col = single(coloring.img);
col(isnan(col)) = 0;



%% prepare everything to pass to mex
 
mdir_field.img(isnan(mdir_field.img)) = 0;
minvsz = min(mdir_field.hdr.dime.pixdim(2:4));
dfield = single(permute(mdir_field.img,[4 1 2 3]));
mask_im = single(mask.img); 
params_s = single([maxfiblen nwalker step/minvsz maxcos sigma gamma]);
theseedmask = single(seedmask.img>thres_seed) .* sum(mdir_field.img.^2,4)>0.0;
seeds = double(find(theseedmask));
if normalize,
    dfield = dfield ./ repmat(eps+sqrt(sum(dfield.^2,1)),[size(dfield,1) 1]);
end
    


%% spectralize
fprintf('num seedvoxels: %i\n',length(seeds));
fprintf('num streamlines to be launched: %i\n',length(seeds)*nwalker);
display('spectralizing')
rr = cppspectralize(dfield*scale,mask_im,col, seeds,params_s); 
result = rr(:,:,:,4:end) /nwalker;
result = result .* repmat(  (single(seedmask.img)-thres_seed)/ (1-thres_seed) , [ 1 1 1 3]);



%% save

template.img = result;
template.hdr.dime.dim(5) = 3;
template.hdr.dime.bitpix = 32;
template.hdr.dime.datatype = 16;

%%
seedmask.img = seedmask.img>thres_seed;
result_cropped =  DPX_cropnifti(template,seedmask,1.1)


%%



save_untouch_nii(result_cropped, outname);

